package main

type GalleryData struct {
	Items []struct {
		Id      int    `json:"id"`
		MediaId string `json:"media_id"`
	} `json:"items"`
}

func (s GalleryData) Len() int {
	return len(s.Items)
}
func (s GalleryData) Swap(i, j int) {
	s.Items[i], s.Items[j] = s.Items[j], s.Items[i]
}
func (s GalleryData) Less(i, j int) bool {
	return s.Items[i].Id < s.Items[j].Id
}

type PreviewSource struct {
	Url string `json:"url"`
}

type Submission struct {
	CrosspostParent     string `json:"crosspost_parent"`
	CrosspostParentList []struct {
		Permalink string `json:"permalink"`
	} `json:"crosspost_parent_list"`
	Url         string `json:"url"`
	IsVideo     bool   `json:"is_video"`
	SecureMedia struct {
		RedditVideo struct {
			HlsUrl      string `json:"hls_url"`
			DashUrl     string `json:"dash_url"`
			FallbackUrl string `json:"fallback_url"`
		} `json:"reddit_video"`
	} `json:"secure_media"`
	IsGallery     bool         `json:"is_gallery"`
	GalleryData   *GalleryData `json:"gallery_data"`
	MediaMetadata map[string]struct {
		Status string `json:"status"`
		S      struct {
			U   string `json:"u"`
			Mp4 string `json:"mp4"`
			Gif string `json:"gif"`
		} `json:"s"`
	} `json:"media_metadata"`
	IsRedditMediaDomain bool `json:"is_reddit_media_domain"`
	Preview             *struct {
		Images []struct {
			Variants struct {
				Mp4 struct {
					Source *PreviewSource `json:"source"`
				} `json:"mp4"`
				Gif struct {
					Source *PreviewSource `json:"source"`
				} `json:"gif"`
			} `json:"variants"`
			Source *PreviewSource `json:"source"`
		} `json:"images"`
	} `json:"preview"`
	IsSelf bool   `json:"is_self"`
	Title  string `json:"title"`
	Domain string `json:"domain"`
}

type SubmissionResponseItem struct {
	Data struct {
		Children []struct {
			Data *Submission `json:"data"`
		} `json:"children"`
	} `json:"data"`
}

type ImgurImage struct {
	Hash string `json:"hash"`
	Ext  string `json:"ext"`
}

type ImgurResponse struct {
	Data struct {
		Images []ImgurImage `json:"images"`
	} `json:"data"`
}

type GfycatResponse struct {
	GfyItem struct {
		Mp4Url string `json:"mp4Url"`
	} `json:"gfyItem"`
}
